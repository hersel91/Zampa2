import config
import MySQLdb
 
def init(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa curiosità"):
			db=MySQLdb.connect(
				config.database['server'],
				config.database['user'],
				config.database['password'],
				config.database['name']
				)
   
			db.autocommit(True)
			db.set_character_set('utf8mb4')
			cur=db.cursor()
 
   
			query = 'SELECT * FROM curiosita ORDER BY RAND() LIMIT 1'
			
			cur.execute(query)
			row=cur.fetchone()
			while row is not None:
				bot.send_message(update.message.chat_id, text=row[1], parse_mode='HTML')
				row = cur.fetchone()
   
				cur.close()
				db.close()
