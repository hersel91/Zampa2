from utils import decorator
import config

@decorator.restricted
@decorator.cancellacomandi
def init(bot, update):
	if update.message.text is not None:
		if update.message.text.lower().startswith("/annuncio"):
			var_messaggio = update.message.text[10:]
			bot.send_message(config.furrydengroup_id, text="<b>Annuncio</b>\n\n{}"
			.format(var_messaggio), 
			parse_mode='HTML')