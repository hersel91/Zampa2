#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Special Thanks to Mirko Brombin & Credit
from telegram.ext import Updater, CommandHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from utils import decorator
import feedparser 
import MySQLdb
import config
import time

'''
Configuration
'''


newsurls = {
    'furryden':       'https://furryden.it/rss',
    'furrydenpinterest':       'https://www.pinterest.it/hersel1991/furrydens.rss',
    #'debianplanet':       'https://planet.debian.org/rss20.xml',
    #'debianweekly':       'https://www.debian.org/News/weekly/dwn.en.rdf',
}

'''
Feed parser
'''
def parseRSS( rss_url ):
    return feedparser.parse(rss_url) 

def getHeadlines(rss_url):
    headlines = [];feed = parseRSS(rss_url)
    for newsitem in feed['items']:
        headlines.append("%s\n%s" % (newsitem['title'], newsitem['link']))
    return headlines

'''
Bot
'''
def build_keyboard(buttons, n_cols, header_buttons=False, footer_buttons=False):
	menu=[buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
	if header_buttons:
		menu.insert(0, header_buttons)
	if footer_buttons:
		menu.append(footer_buttons)
	return InlineKeyboardMarkup(menu)

@decorator.restricted
@decorator.private
def fetch(bot, update):
    
        db=MySQLdb.connect(
				    config.database['server'],
				    config.database['user'],
				    config.database['password'],
				    config.database['name']
				    )
        db.autocommit(True)
        cur=db.cursor()

        allheadlines = []
        for key,url in newsurls.items():
            allheadlines.extend(getHeadlines(url))

        for hl in allheadlines:
            feedlink = hl.split("\n")[1]
            cur.execute("SELECT * FROM FeedItems WHERE Item = %s", [feedlink])
            row = cur.fetchall()
            if cur.rowcount == 0:
                buttons = []
                buttons.append(InlineKeyboardButton("Discuti", url='https://t.me/officialfurryden'))
                markup = build_keyboard(buttons, n_cols=1)
                bot.send_message(config.channel_rss, hl, reply_markup=markup)
                
                cur.execute("INSERT INTO FeedItems (Item) VALUES (%s)", [feedlink])
                time.sleep(5)
        cur.close(); db.close()