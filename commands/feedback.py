import config
from utils import decorator

@decorator.cancellacomandi
@decorator.private
def init(bot, update):
	if update.message is not None and update.message.text is not None:
		if update.message.text.startswith("/feedback"):

			var_messaggio = update.message.text
			var_messaggio = update.message.text[9:]
			bot.send_message(config.admingroup_id, 
                 text="<b>FEEDBACK:</b>\n<code>Messaggio:{}</code>"
					 .format(var_messaggio),
                 parse_mode='HTML')
			bot.send_message(update.message.from_user.id, 
				 text='Feedback inviato correttamente!', 
				 parse_mode='HTML')