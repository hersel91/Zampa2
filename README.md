<img src="https://i2.wp.com/furryden.it/wp-content/uploads/2018/07/CHI-SIAMO1_sito.png?ssl=1" alt="Smiley face" height="297" width="800">

[![Python3.7+](https://img.shields.io/badge/Python-3.7%2B-green.svg)](https://www.python.org/downloads) [![Group](https://img.shields.io/badge/Group-FurryDen-blue)](https://t.me/officialfurryden) [![License: GPL 3.0](https://img.shields.io/badge/License-GPL%203.0-brightgreen)](https://github.com/hersel91/Zampa2/blob/master/LICENSE)

Use =>https://python-telegram-bot.org
<br>
Use => import MySQLdb https://pythonspot.com/mysql-with-python/
<br>
<br>
By Hersel Giannella => https://hersel.it
<br>
<b>Special Thanks to my sensei Mirko Brombin</b>
<br>
Thanks also to => http://github.com/PsykeDady
<br>
Thanks also to => https://github.com/Felinesec/FelineSec-Bot
<br>
## Variants of the bot
NebulaBot => https://github.com/hersel91/nebulabot
<br>
### Optional ban command:
=> /commands/ban.py
```python
from utils import decorator
@decorator.restricted
@decorator.cancellacomandi
def init(bot, update):
    if update.message.text is not None:
        if update.message.text.startswith("/ban"):
            bot.kick_chat_member(update.message.chat_id, update.message.reply_to_message.from_user.id)
```
            
### Documentation
[ITA] https://hersel.it/zampadoc
