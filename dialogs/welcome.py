import config
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from utils import util
#BENVENUTO DI ZAMPA
def init(bot, update):
	if update.message is not None and update.message.new_chat_members is not None:
		for new in update.message.new_chat_members:
			button_list = [
	    	InlineKeyboardButton("SITO", url='https://furryden.it'),
	    	InlineKeyboardButton("REGOLE", url='https://telegra.ph/Regolamento-di-FurryDen-06-17'),
	    	InlineKeyboardButton("PROGETTO", url='https://furryden.it/progetto/')]
			
	
			reply_markup = InlineKeyboardMarkup(util.build_menu(button_list, n_cols=2))
			update.message.reply_text('Benvenuto {username} in {chat_title}!\nRicordati di leggere il regolamento /regole\ne buona permanenza\nCanale SFW: /canale\nCanale NSFW: /nsfw\nPer richiedere supporto digita: @admin tuomessaggio'
				.format(username= "@"+new.username, chat_title = update.message.chat.title), reply_markup=reply_markup)
			bot.send_message(config.admingroup_id, text="NUOVO INGRESSO NEL GRUPPO!\nUSERNAME: {username}\nID: {idutente}\nGRUPPO: {chat_title}\nAGGIUNTO DA: {nome} ".format(username="@"+new.username, 
				chat_title=update.message.chat.title, 
				idutente=update.message.from_user.id, 
				nome="@"+update.message.from_user.username), 
				parse_mode='HTML')