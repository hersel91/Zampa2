import random
import config
from utils import decorator
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
import telegram
from time import sleep
from random import random
from . import definisci
from . import welcome
from . import admincommand
from . import battuta
from . import customhandler
from . import customhandler2
from . import curiosita
from commands import button 
from utils import util

#RISPOSTE DI ZAMPA
def zampa(bot, update):
	if update.message is not None and update.message.text is not None: 
		if str(update.message.text).lower().startswith("zampa dammi la zampa"):
			bot.send_message(update.message.chat_id, text="ERROR ERROR ERROR ERROR {username} Designato come virus❗️".format(username=update.message.from_user.first_name), parse_mode='HTML')
#pompino
def pompino(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa fammi un pompino"):
			bot.send_message(update.message.chat_id, text="{username} fattelo da solo! non sei di certo alla mia altezza quindi smamma❗️".format(username=update.message.from_user.first_name), parse_mode='HTML')
#zizzania
def zizzania(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("/zizzania"):
			bot.send_message(update.message.chat_id, text="{username} non sono spacobot imbecille.".format(username=update.message.from_user.first_name), parse_mode='HTML')
#bocchino
def bocchino(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa fammi un bocchino"):
			bot.send_message(update.message.chat_id, text="{username} fattelo da solo! non sei di certo alla mia altezza quindi smamma❗️".format(username=update.message.from_user.first_name), parse_mode='HTML')
#panino
def panino(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa fammi un panino"):
			bot.send_message(update.message.chat_id, text="{username} fattelo da solo❗️".format(username=update.message.from_user.first_name), parse_mode='HTML')
#panino
@decorator.ownerbot
def panino2(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("sudo zampa fammi un panino"):
			bot.send_message(update.message.chat_id, text="subito {username} mio padrone❗️".format(username=update.message.from_user.first_name), parse_mode='HTML')
#easter eggs
def egg(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("0111101001100001011011010111000001100001"):
			bot.send_message(update.message.chat_id, text="https://hersel.it/egg".format(username=update.message.from_user.first_name), parse_mode='HTML')
#PRESENTAZIONE ZAMPA
def presentazione(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa chi sei?"):
			bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/refsheet2.jpg', caption='Ciao sono Zampa! la mascotte di FurryDen\nrefsheet in HD: https://furryden.it/immagini/refsheet.png')

def presentazione2(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("chi sei zampa?"):
			bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/refsheet2.jpg', caption='Ciao sono Zampa! la mascotte di FurryDen\nrefsheet in HD: https://furryden.it/immagini/refsheet.png')
			
#grattino2
def grattino2(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("*fa un grattino a zampa*"):
			bot.send_message(update.message.chat_id, text="*fa un grattino a {username}*".format(username=update.message.from_user.first_name), parse_mode='HTML')
#grattino2
def kaffe(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("buongiornissimo"):
			bot.send_message(update.message.chat_id, text="Kaffeeee?!", parse_mode='HTML')
#grattino
def grattino(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa fammi un grattino"):
			bot.send_message(update.message.chat_id, text="*fa un grattino a {username}*".format(username=update.message.from_user.first_name), parse_mode='HTML')
#php
def zampaphp(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa ti piace il php?"):
			bot.send_message(update.message.chat_id, text="{username} il PHP mi provoca stati di dissenteria cronica.".format(username=update.message.from_user.first_name), parse_mode='HTML')
#vaffanculo
def zampafanculo(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa sei una testa di cazzo"):
			bot.send_message(update.message.chat_id, text="{username} solo un coglione come te poteva riconoscere il suo compagno perduto".format(username=update.message.from_user.first_name), parse_mode='HTML')

def dammizampa(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("ciao zampa"):
			bot.send_message(update.message.chat_id, "Ciao {username}".format(username=update.message.from_user.first_name))
def buonasera(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("buonasera"):
			bot.send_message(update.message.chat_id, "buonasera {username}".format(username=update.message.from_user.first_name))

#Buongiorno
def buongiorno(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("buongiorno"):
			bot.sendSticker(update.message.chat_id, 'https://furryden.it/immagini/stickerbot/zampagiorno.webp')
			bot.send_message(update.message.chat_id, "Buongiorno {username}".format(username=update.message.from_user.first_name))
#Buonanotte
def buonanotte(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("buonanotte"):
			bot.sendSticker(update.message.chat_id, 'https://furryden.it/immagini/stickerbot/zampanight.webp')
			bot.send_message(update.message.chat_id, "Buonanotte {username}".format(username=update.message.from_user.first_name))
#avada kedavra
def kedavra(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("avada kedavra"):
			update.message.reply_text("{username} Avada Affanculo".format(username=update.message.from_user.first_name))
#send nudes
def sendnudes(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa send nudes"):
			bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/sendnudes2.png')
#come stai
def comestai(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("come stai zampa?"):
			bot.send_message(update.message.chat_id, "Sto bene {username}".format(username=update.message.from_user.first_name))
#play despacito
def despacito(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("zampa play despacito"):
			bot.send_audio(update.message.chat_id, audio='https://furryden.it/datastore/audio/audiodespacito.mp3')
#GROOT
def groot(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("io sono groot"):
			bot.send_photo(update.message.chat_id, 
			photo='https://nerdist.com/wp-content/uploads/2018/03/Dancing-Groot-6-1024x716.jpg', 
			caption="IO SONO GROOT", 
			parse_mode='HTML'
			)



#DICHIARAZIONE FUNZIONI
def init(bot, update):
	zampa(bot, update)
	buongiorno(bot, update)
	buonanotte(bot, update)
	dammizampa(bot, update)
	kedavra(bot, update)
	sendnudes(bot, update)
	comestai(bot, update)
	buonasera(bot, update)
	grattino(bot, update)
	pompino(bot, update)
	bocchino(bot, update)
	grattino2(bot, update)
	panino(bot, update)
	panino2(bot, update)
	kaffe(bot, update)
	presentazione(bot, update)
	presentazione2(bot, update)
	zampaphp(bot, update)
	zampafanculo(bot, update)
	zizzania(bot, update)
	despacito(bot, update)
	egg(bot, update)
	groot(bot, update)
	util.debug(update)
	#INIT COMMAND
	definisci.init(bot, update)
	welcome.init(bot, update)
	admincommand.init(bot, update)
	battuta.init(bot, update)
	customhandler.init(bot, update)
	customhandler2.init(bot, update)
	curiosita.init(bot, update)


	
	