import config
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup

def init(bot, update):
	if update.message is not None and update.message.text is not None:
		if update.message.text.startswith("@admin"):
			keyboard = [[InlineKeyboardButton("Risolto✅", callback_data='1')]]
			reply_markup = InlineKeyboardMarkup(keyboard)
			

			var_messaggio = update.message.text
			var_messaggio = update.message.text[7:]
			bot.send_message(config.admingroup_id, 
                 text="<b>NUOVA RICHIESTA DI SUPPORTO</b>\nAutore: {username}\nLink: {linkurl}/{link}\n<code>Messaggio:{}</code>"
					 .format(var_messaggio, 
					 username="@"+update.message.from_user.username, 
					 link=update.message.message_id, 
					 linkurl="https://t.me/c/1122618996"),
                 parse_mode='HTML', reply_markup=reply_markup)
def risolto(bot, update):
	query = update.callback_query
	var_messaggio = query.message.text
	var_messaggio = query.message.text[6:]
	query.edit_message_text(text="{}\n<b>Risolto da: @{username}</b>"
	.format(var_messaggio,username=str(update.effective_user.username)),parse_mode='HTML')
