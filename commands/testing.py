import config
import MySQLdb
from utils import decorator
 
@decorator.ownerbot
def init(bot, update):
	if update.message is not None and update.message.text is not None:
		if str(update.message.text).lower().startswith("/wordpress"):
			db=MySQLdb.connect(
                config.database2['server'],
                config.database2['user'],
                config.database2['password'],
                config.database2['name']
                )
   
			db.autocommit(True)
			db.set_character_set('utf8mb4')
			cur=db.cursor()
 
   
			query = 'SELECT * FROM wp_users ORDER BY RAND() LIMIT 1'
			
			cur.execute(query)
			row=cur.fetchone()
			while row is not None:
				
				bot.send_message(update.message.chat_id, text=row[1])
				row = cur.fetchone()
   
				cur.close()
				db.close()