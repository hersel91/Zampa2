#!/usr/bin/env python3
#   rules.py
#   Python 3.7
#   Version 0.1
#
#   Created by Talebian
#   Mozilla Public License
#

import json
from datetime import datetime
from utils import decorator

@decorator.cancellacomandi
def init(bot, update):
    with open('json-data/aiuto.json') as rules_js:
        data = json.load(rules_js)

    rules = "<b>AIUTO:</b>\n{rules}".format(rules=data['rules'])

    bot.send_message(update.message.chat_id,text=rules, parse_mode='HTML')