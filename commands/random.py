import random
import os
from utils import decorator

@decorator.cancellacomandi
def init(bot, update):
    directory = "/home/hersel/pythonserver/zampabot/img/"
    random_image = random.choice(os.listdir(directory))
    bot.send_photo(update.message.chat_id, photo=open(directory + random_image,'rb'),
    caption="Visita https://t.me/furrygallery per i crediti", 
    parse_mode='HTML')