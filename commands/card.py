from utils import decorator
#COMANDI CARD
@decorator.cancellacomandi
def hersy(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/hersy.png')
@decorator.cancellacomandi
def ryanking(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/ryan.png')
@decorator.cancellacomandi
def degron(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/degron.png')
@decorator.cancellacomandi
def leo(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/andrea.png')
@decorator.cancellacomandi
def ren(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/ren.png')
@decorator.cancellacomandi
def glxxce(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/giorgia.png')
@decorator.cancellacomandi
def miki(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/mikiinverno.png')
@decorator.cancellacomandi
def miki2(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/mikiestate.png')
@decorator.cancellacomandi
def dave(bot, update):
    bot.send_photo(update.message.chat_id, 'https://furryden.it/immagini/card/dave3.png')
#COMANDI CARD END